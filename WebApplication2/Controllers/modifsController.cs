﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class modifsController : Controller
    {
        private WSEntities db = new WSEntities();

        // GET: modifs
        public ActionResult Index()
        {
            return View(db.modif.ToList());
        }

        // GET: modifs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            modif modif = db.modif.Find(id);
            if (modif == null)
            {
                return HttpNotFound();
            }
            return View(modif);
        }

        // GET: modifs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: modifs/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "modif_id,modif_name")] modif modif)
        {
            if (ModelState.IsValid)
            {
                db.modif.Add(modif);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(modif);
        }

        // GET: modifs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            modif modif = db.modif.Find(id);
            if (modif == null)
            {
                return HttpNotFound();
            }
            return View(modif);
        }

        // POST: modifs/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "modif_id,modif_name")] modif modif)
        {
            if (ModelState.IsValid)
            {
                db.Entry(modif).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(modif);
        }

        // GET: modifs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            modif modif = db.modif.Find(id);
            if (modif == null)
            {
                return HttpNotFound();
            }
            return View(modif);
        }

        // POST: modifs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            modif modif = db.modif.Find(id);
            db.modif.Remove(modif);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
