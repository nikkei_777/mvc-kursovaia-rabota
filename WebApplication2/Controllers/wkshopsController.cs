﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class wkshopsController : Controller
    {
        private WSEntities db = new WSEntities();

        // GET: wkshops
        public ActionResult Index()
        {
            return View(db.wkshop.ToList());
        }

        // GET: wkshops/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wkshop wkshop = db.wkshop.Find(id);
            if (wkshop == null)
            {
                return HttpNotFound();
            }
            return View(wkshop);
        }

        // GET: wkshops/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: wkshops/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "wkshop_id,wkshop_name,whshop_square")] wkshop wkshop)
        {
            if (ModelState.IsValid)
            {
                db.wkshop.Add(wkshop);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(wkshop);
        }

        // GET: wkshops/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wkshop wkshop = db.wkshop.Find(id);
            if (wkshop == null)
            {
                return HttpNotFound();
            }
            return View(wkshop);
        }

        // POST: wkshops/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "wkshop_id,wkshop_name,whshop_square")] wkshop wkshop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wkshop).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(wkshop);
        }

        // GET: wkshops/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wkshop wkshop = db.wkshop.Find(id);
            if (wkshop == null)
            {
                return HttpNotFound();
            }
            return View(wkshop);
        }

        // POST: wkshops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            wkshop wkshop = db.wkshop.Find(id);
            db.wkshop.Remove(wkshop);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
