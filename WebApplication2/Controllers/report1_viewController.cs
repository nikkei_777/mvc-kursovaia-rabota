﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class report1_viewController : Controller
    {
        private WSEntities db = new WSEntities();
        private IzgotDetEntities view = new IzgotDetEntities();

        public List<Array> resultList = new List<Array>();

        public ActionResult Index()
        {
           ObjectResult<report1_Result> result = view.report1();
            
            foreach (var item in result)
            {
                resultList.Add(new string[] { item.Название_детали, item.Название_модификации, Convert.ToString(item.Трудоемкость) });
            }

            ViewBag.result = resultList;

            return View();

        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
