﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class report2_viewController : Controller
    {
        private WSEntities db = new WSEntities();
        public List<Array> resultList = new List<Array>();
        public List<string> wkShopsList = new List<string>();

        // GET: Doc2
        public ActionResult Index()
        {
            string wkShop = Request.QueryString["name"];
            string letter = Request.QueryString["letter"];

            if (wkShop != null && letter != null)
            {

                var queryResults = from dm in db.detal_on_machine
                                   join d in db.detal on dm.detal_id equals d.detal_id
                                   join m in db.machine on dm.machine_id equals m.machine_id
                                   join w in db.wkshop on m.wkshop_id equals w.wkshop_id
                                   where (w.wkshop_name == wkShop && m.machine_name.Substring(0, 1) == letter)
                                   orderby dm.time
                                   select new { wkShop_name = w.wkshop_name, detail_name = d.detal_name, machine_name = m.machine_name, time = dm.time };
                foreach (var item in queryResults)
                {
                    resultList.Add(new string[] { item.machine_name, item.detail_name, item.wkShop_name, Convert.ToString(item.time) });
                }
            }

            var wkshops = from w in db.wkshop
                          select new { wkShop_name1 = w.wkshop_name };

            foreach (var item in wkshops)
            {
                wkShopsList.Add(item.wkShop_name1);
            }

            ViewBag.wkShops = wkShopsList;
            ViewBag.result = resultList;

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
