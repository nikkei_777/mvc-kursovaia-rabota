﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class detal_on_machineController : Controller
    {
        private WSEntities db = new WSEntities();

        // GET: detal_on_machine
        public ActionResult Index()
        {
            var detal_on_machine = db.detal_on_machine.Include(d => d.detal).Include(d => d.machine);
            return View(detal_on_machine.ToList());
        }

        // GET: detal_on_machine/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detal_on_machine detal_on_machine = db.detal_on_machine.Find(id);
            if (detal_on_machine == null)
            {
                return HttpNotFound();
            }
            return View(detal_on_machine);
        }

        // GET: detal_on_machine/Create
        public ActionResult Create()
        {
            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name");
            ViewBag.machine_id = new SelectList(db.machine, "machine_id", "machine_name");
            return View();
        }

        // POST: detal_on_machine/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,machine_id,detal_id,time")] detal_on_machine detal_on_machine)
        {
            if (ModelState.IsValid)
            {
                db.detal_on_machine.Add(detal_on_machine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name", detal_on_machine.detal_id);
            ViewBag.machine_id = new SelectList(db.machine, "machine_id", "machine_name", detal_on_machine.machine_id);
            return View(detal_on_machine);
        }

        // GET: detal_on_machine/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detal_on_machine detal_on_machine = db.detal_on_machine.Find(id);
            if (detal_on_machine == null)
            {
                return HttpNotFound();
            }
            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name", detal_on_machine.detal_id);
            ViewBag.machine_id = new SelectList(db.machine, "machine_id", "machine_name", detal_on_machine.machine_id);
            return View(detal_on_machine);
        }

        // POST: detal_on_machine/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,machine_id,detal_id,time")] detal_on_machine detal_on_machine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detal_on_machine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name", detal_on_machine.detal_id);
            ViewBag.machine_id = new SelectList(db.machine, "machine_id", "machine_name", detal_on_machine.machine_id);
            return View(detal_on_machine);
        }

        // GET: detal_on_machine/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detal_on_machine detal_on_machine = db.detal_on_machine.Find(id);
            if (detal_on_machine == null)
            {
                return HttpNotFound();
            }
            return View(detal_on_machine);
        }

        // POST: detal_on_machine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            detal_on_machine detal_on_machine = db.detal_on_machine.Find(id);
            db.detal_on_machine.Remove(detal_on_machine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
