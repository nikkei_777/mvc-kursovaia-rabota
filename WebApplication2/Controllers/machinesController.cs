﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class machinesController : Controller
    {
        private WSEntities db = new WSEntities();

        // GET: machines
        public ActionResult Index()
        {
            var machine = db.machine.Include(m => m.wkshop);
            return View(machine.ToList());
        }

        // GET: machines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            machine machine = db.machine.Find(id);
            if (machine == null)
            {
                return HttpNotFound();
            }
            return View(machine);
        }

        // GET: machines/Create
        public ActionResult Create()
        {
            ViewBag.wkshop_id = new SelectList(db.wkshop, "wkshop_id", "wkshop_name");
            return View();
        }

        // POST: machines/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "machine_id,wkshop_id,machine_name")] machine machine)
        {
            if (ModelState.IsValid)
            {
                db.machine.Add(machine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.wkshop_id = new SelectList(db.wkshop, "wkshop_id", "wkshop_name", machine.wkshop_id);
            return View(machine);
        }

        // GET: machines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            machine machine = db.machine.Find(id);
            if (machine == null)
            {
                return HttpNotFound();
            }
            ViewBag.wkshop_id = new SelectList(db.wkshop, "wkshop_id", "wkshop_name", machine.wkshop_id);
            return View(machine);
        }

        // POST: machines/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "machine_id,wkshop_id,machine_name")] machine machine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(machine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.wkshop_id = new SelectList(db.wkshop, "wkshop_id", "wkshop_name", machine.wkshop_id);
            return View(machine);
        }

        // GET: machines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            machine machine = db.machine.Find(id);
            if (machine == null)
            {
                return HttpNotFound();
            }
            return View(machine);
        }

        // POST: machines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            machine machine = db.machine.Find(id);
            db.machine.Remove(machine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
