﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class modif_to_detalController : Controller
    {
        private WSEntities db = new WSEntities();

        // GET: modif_to_detal
        public ActionResult Index()
        {
            var modif_to_detal = db.modif_to_detal.Include(m => m.detal).Include(m => m.modif);
            return View(modif_to_detal.ToList());
        }

        // GET: modif_to_detal/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            modif_to_detal modif_to_detal = db.modif_to_detal.Find(id);
            if (modif_to_detal == null)
            {
                return HttpNotFound();
            }
            return View(modif_to_detal);
        }

        // GET: modif_to_detal/Create
        public ActionResult Create()
        {
            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name");
            ViewBag.modif_id = new SelectList(db.modif, "modif_id", "modif_name");
            return View();
        }

        // POST: modif_to_detal/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,modif_id,detal_id,cost")] modif_to_detal modif_to_detal)
        {
            if (ModelState.IsValid)
            {
                db.modif_to_detal.Add(modif_to_detal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name", modif_to_detal.detal_id);
            ViewBag.modif_id = new SelectList(db.modif, "modif_id", "modif_name", modif_to_detal.modif_id);
            return View(modif_to_detal);
        }

        // GET: modif_to_detal/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            modif_to_detal modif_to_detal = db.modif_to_detal.Find(id);
            if (modif_to_detal == null)
            {
                return HttpNotFound();
            }
            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name", modif_to_detal.detal_id);
            ViewBag.modif_id = new SelectList(db.modif, "modif_id", "modif_name", modif_to_detal.modif_id);
            return View(modif_to_detal);
        }

        // POST: modif_to_detal/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,modif_id,detal_id,cost")] modif_to_detal modif_to_detal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(modif_to_detal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.detal_id = new SelectList(db.detal, "detal_id", "detal_name", modif_to_detal.detal_id);
            ViewBag.modif_id = new SelectList(db.modif, "modif_id", "modif_name", modif_to_detal.modif_id);
            return View(modif_to_detal);
        }

        // GET: modif_to_detal/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            modif_to_detal modif_to_detal = db.modif_to_detal.Find(id);
            if (modif_to_detal == null)
            {
                return HttpNotFound();
            }
            return View(modif_to_detal);
        }

        // POST: modif_to_detal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            modif_to_detal modif_to_detal = db.modif_to_detal.Find(id);
            db.modif_to_detal.Remove(modif_to_detal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
