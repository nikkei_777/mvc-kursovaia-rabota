﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class detalsController : Controller
    {
        private WSEntities db = new WSEntities();

        // GET: detals
        public ActionResult Index()
        {
            return View(db.detal.ToList());
        }

        // GET: detals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detal detal = db.detal.Find(id);
            if (detal == null)
            {
                return HttpNotFound();
            }
            return View(detal);
        }

        // GET: detals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: detals/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "detal_id,detal_name,detal_firm")] detal detal)
        {
            if (ModelState.IsValid)
            {
                db.detal.Add(detal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(detal);
        }

        // GET: detals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detal detal = db.detal.Find(id);
            if (detal == null)
            {
                return HttpNotFound();
            }
            return View(detal);
        }

        // POST: detals/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "detal_id,detal_name,detal_firm")] detal detal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(detal);
        }

        // GET: detals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detal detal = db.detal.Find(id);
            if (detal == null)
            {
                return HttpNotFound();
            }
            return View(detal);
        }

        // POST: detals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            detal detal = db.detal.Find(id);
            db.detal.Remove(detal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
